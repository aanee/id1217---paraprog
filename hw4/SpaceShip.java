import java.util.concurrent.ThreadLocalRandom;

public class SpaceShip implements Runnable {
    Thread thread;
    private String thread_id;
    private Station station;
    private int n_fuel_need;
    private int q_fuel_need;

    private boolean supply_ship;
    private int n_supply;
    private int q_supply;

    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_RED = "\u001B[31m";

    SpaceShip (String id, boolean _supply_ship, Station _st) {
        thread_id = id;
        supply_ship = _supply_ship;
        station = _st;
        if (!supply_ship) {
            n_fuel_need = ThreadLocalRandom.current().nextInt(5, 50);
            q_fuel_need = ThreadLocalRandom.current().nextInt(5, 50);
        } else {
            switch (ThreadLocalRandom.current().nextInt(0,2)) {
                case 0:
                    n_fuel_need = 75;
                    q_fuel_need = 0;
                    break;
                case 1:
                    n_fuel_need = 0;
                    q_fuel_need = 30;
                    break;
                default:
                    n_fuel_need = 50;
                    q_fuel_need = 25;
                    break;
            }

            if (ThreadLocalRandom.current().nextBoolean()) {
                n_supply = 3000;
                q_supply = 0;
            } else {
                n_supply = 0;
                q_supply = 1000;
            }
        }
    }

    private void busy_doing_nothing (int start, int end) throws InterruptedException {
        int t = ThreadLocalRandom.current().nextInt(start, end);
        Thread.sleep(t * FuelSpaceStation.mult);
    }

    @Override
    public void run () {
        int i = 0, t, start = 100, end = 200;

        while (++i <= FuelSpaceStation.runs) {
            try {
                // sleep - goes to station
                busy_doing_nothing(start, end);

                // Check if space for fuel in station
                if (supply_ship){
                    System.out.println(ANSI_PURPLE + thread_id + " Checking Fuel Space in Station" + ANSI_PURPLE);
                    station.checkReadyForSupply(n_supply, q_supply);
                }

                // request docking
                System.out.println(ANSI_BLUE + thread_id + " Request Docking" + ANSI_BLUE);
                station.requestDocking(supply_ship);

                if (supply_ship) {
                    // refuel station
                    System.out.println(ANSI_PURPLE + thread_id + " Refueling Station" + ANSI_PURPLE);
                    station.refuelStation(n_supply, q_supply);
                    busy_doing_nothing(start, end);
                }

                // refuel ship
                System.out.println(ANSI_BLUE + thread_id + " Refueling Ship" + ANSI_BLUE);
                station.fuelShipN(n_fuel_need, q_fuel_need);
                busy_doing_nothing(5, 10);

                // undock
                System.out.println(ANSI_BLUE + thread_id + " Undocking" + ANSI_BLUE);
                station.requestUnDocking();

                // sleep - goes back to whenst it came
                busy_doing_nothing(start, end);
            } catch (InterruptedException e) {}
        }
        System.out.println(ANSI_RED + thread_id + " DONE" + ANSI_RED);

    }

    public void start () {
        System.out.println(thread_id + " Started");
        if (thread == null) {
            thread = new Thread(this, thread_id);
            thread.start();
        }
    }
}