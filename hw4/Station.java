/*
Potential improvements: Separate Station = fuel logic from FlightControl = docking logic
 */

class Station {
    private int docked = 0;
    private int n_fuel = 0;
    private int q_fuel = 0;
    private int docks = 10;
    private int max_n_fuel = 30000;
    private int max_q_fuel = 10000;
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";

    Station(int _docks){
        docks = _docks;
    }
    Station(int _docks, int _q_fuel, int _n_fuel, int _max_q_fuel, int _max_n_fuel){
        docks = _docks;
        q_fuel = _q_fuel;
        n_fuel = _n_fuel;
        max_q_fuel = _max_q_fuel;
        max_n_fuel = _max_q_fuel;
    }

    synchronized void requestDocking(boolean supply) throws InterruptedException{
        // if no space for docking place in queue
        while (docked == docks) wait();
        while ((n_fuel < 100 || q_fuel < 100) && !supply) wait();
        docked++;
    }

    synchronized void requestUnDocking () throws InterruptedException{
        while (docked == 0)
            wait();
        docked--;
        notifyAll();
    }

    synchronized void refuelStation (int n_input_fuel, int q_input_fuel) throws InterruptedException {
        while ( n_input_fuel > checkFuelSpaceN() || q_input_fuel > checkFuelSpaceQ()) wait();
        n_fuel += n_input_fuel;
        q_fuel += q_input_fuel;
        System.out.println(ANSI_GREEN + "N:" + n_fuel + " Q:" + q_fuel + ANSI_GREEN);
        notifyAll();
    }

    synchronized void fuelShipN (int n_output_fuel, int q_output_fuel) throws InterruptedException{
        while ( n_output_fuel > n_fuel || q_output_fuel > q_fuel) wait();
        n_fuel -= n_output_fuel;
        q_fuel -= q_output_fuel;
        System.out.println(ANSI_RED + "N:" + n_fuel + " Q:" + q_fuel + ANSI_RED);
        notifyAll();
    }

    synchronized void checkReadyForSupply(int n_input_fuel, int q_input_fuel) throws InterruptedException {
        while ( n_input_fuel > checkFuelSpaceN() || q_input_fuel > checkFuelSpaceQ()) wait();
    }

    private synchronized int checkFuelSpaceN () {
        return max_n_fuel-n_fuel;
    }

    private synchronized int checkFuelSpaceQ () {
        return max_q_fuel-q_fuel;
    }
}