cmake_minimum_required(VERSION 3.13)
project(hw1 C)

set(CMAKE_C_STANDARD 11)
SET(CMAKE_C_FLAGS ${CMAKE_C_FLAGS} "-pthread")

add_executable(hw1
        #wc.c
        infinite_wc.c
        )
