/*
 * semaphoers should be used
 *
 * Suppose there is only one bathroom in your department. It can be used by any 
 * number of men or any number of women, but not at the same time. Develop and
 * implement a multithreaded program that provides a fair solution to this problem 
 * using only semaphores for synchronization. Represent men and women as threads
 * that repeatedly work (sleep for a random amount of time) and use the bathroom. 
 * Your program should allow any number of men or women to be in the bathroom at 
 * the same time. Your solution should ensure the required exclusion and avoid
 * deadlock, and ensure fairness, i.e. ensure that any person (man or woman) which 
 * is waiting to enter the bathroom eventually gets to do so. Have the persons
 * sleep for a random amount of time between visits to the bathroom and have them
 * sleep for a smaller random amount of time to simulate the time it takes to be in
 * the bathroom. Your program should print a trace of interesting simulation
 * events. See the Readers/Writers problem discussed in Lecture 10: Semaphores.
 *
 * */

#ifndef _REENTRANT
#define _REENTRANT
#endif
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdbool.h>

#define SHARED 1
#define MULT 0.01
#define TURNS 4

#define W 4
#define M 4

typedef struct queue_t {
    struct queue_t *next;
    int id;
} queue_t;

static queue_t queue_w = {NULL, -1};
static queue_t *first_w = &queue_w;
static queue_t *last_w = &queue_w;

static queue_t queue_m = {NULL, -1};
static queue_t *first_m = &queue_m;
static queue_t *last_m = &queue_m;

sem_t empty_sem, queue_w_sem, queue_m_sem, going_sem;
pthread_t w_id[W];
pthread_t m_id[M];

int go = 0, turn = 0, going = 0;

void queue_sem_in (int s) {
    if(s) sem_wait(&queue_m_sem);
    else sem_wait(&queue_w_sem);
}

void queue_sem_out (int s) {
    if(s) sem_post(&queue_m_sem);
    else sem_post(&queue_w_sem);
}

void* goer (void* args) {
    int i = TURNS;
    int *p = (int*) args;
    int id = p[0];
    int s = p[1];
    queue_t *first, *last, *visitor = (queue_t *) malloc(sizeof(queue_t));
    visitor->id = id;

    if (s) {
        first = first_m;
        last = last_m;
    } else {
        first = first_w;
        last = last_w;
    }
    
    while (true) {
        if (s) printf("M%d: I'm alive\n", id);
        else printf("W%d: I'm alive\n", id);

        sleep((unsigned) (rand()%1000 * MULT));

        queue_sem_in(s);
        visitor->next = NULL;
        last->next = visitor;
        last = last->next;
        if (first->id == -1) first = first->next;
        queue_sem_out(s);

        if (s) printf("M%d: In queue\n", id);
        else printf("W%d: In queue\n", id);

        if (id == 0 && s == 0 && i-- == TURNS) sem_post(&empty_sem);

        // Sure you got here, but are you next in line?
        while(true) {
            queue_sem_in(s);
            if (go && turn == s && first->id == id) {
                if (first->next != NULL) first = first->next;
                else {
                    last = (s) ? &queue_m : &queue_w;
                    first = (s) ? &queue_m : &queue_w;
                    first->next = NULL;
                }
                queue_sem_out(s);
                break;
            } else queue_sem_out(s);
        }
        
        if (s) printf("M%d: My turn!\n", id);
        else printf("W%d: My turn!\n", id);


        // How many made it?
        sem_wait(&going_sem);
        going++;
        sem_post(&going_sem);
 
        // congratulations your're in.
        sleep((unsigned) (rand()%1000 * MULT));

        sem_wait(&going_sem);
        if (going == 1) {
            sem_post(&empty_sem);
        }
        going--;
        sem_post(&going_sem);

        if (s) printf("=== M%d: I'm out\n", id);
        else printf("### W%d: I'm out\n", id);
    }
}

int main (int argc, char** argv) {
    int i, turns = TURNS;
    int args[W+M][2];

    sem_init(&empty_sem, SHARED, 0);
    sem_init(&queue_w_sem, SHARED, 1);
    sem_init(&queue_m_sem, SHARED, 1);
    sem_init(&going_sem, SHARED, 1);

    for (i = 0; i < W; i++) {
        args[i][0] = i;
        args[i][1] = 0;
        pthread_create(&w_id[i], NULL, goer, args[i]);
    }

    for (i = 0; i < M; i++) {
        args[W+i][0] = i;
        args[W+i][1] = 1;
        pthread_create(&m_id[i], NULL, goer, args[W+i]);
    }

    // timer logic here
    while(turns--) {
    
        go = 1;
        sleep(1);
        if (turns == TURNS) sem_wait(&empty_sem);
        go = 0;
        sem_wait(&empty_sem);
        if(turn) turn = 0;
        else turn = 1;
    }

    printf("KILLING CHILDREN\n");
    for (i = 0; i < W; i++) {
        pthread_cancel(w_id[i]);
        pthread_cancel(m_id[i]);
    }
    printf("DONE\n");
    pthread_exit(NULL);
}
