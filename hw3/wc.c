/*
 * semaphoers should be used
 *
 * Suppose there is only one bathroom in your department. It can be used by any 
 * number of men or any number of women, but not at the same time. Develop and 
 * implement a multithreaded program that provides a fair solution to this problem 
 * using only semaphores for synchronization. Represent men and women as threads 
 * that repeatedly work (sleep for a random amount of time) and use the bathroom. 
 * Your program should allow any number of men or women to be in the bathroom at 
 * the same time. Your solution should ensure the required exclusion and avoid 
 * deadlock, and ensure fairness, i.e. ensure that any person (man or woman) which 
 * is waiting to enter the bathroom eventually gets to do so. Have the persons 
 * sleep for a random amount of time between visits to the bathroom and have them 
 * sleep for a smaller random amount of time to simulate the time it takes to be in
 * the bathroom. Your program should print a trace of interesting simulation 
 * events. See the Readers/Writers problem discussed in Lecture 10: Semaphores.
 *
 * */
#ifndef _REENTRANT
#define _REENTRANT
#endif
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdbool.h>
#define SHARED 1

#define MAXTOILETS 5 
#define MAXPEOPLE 20
#define MAXITERATIONS 5 
#define MULT 0.001

pthread_t goer_id[MAXPEOPLE];
sem_t bathroom, queue_sem;
int iterations, toilets;

typedef struct queue_t {
    int id;
    struct queue_t *next;
} queue_t;

static queue_t queue = {-1, NULL};
static queue_t *first = &queue;
static queue_t *last = &queue;

void* goer (void* args) {
    int i = 0, id = (int) args;
    queue_t *visitor = (queue_t *) malloc(sizeof(queue_t));
    visitor->id = id;

    while (i++ < iterations) {

        printf("%d: I'm alive!\n", id);
        // sleep for s seconds floating point is allowed.
        sleep((unsigned) (rand()%1000 * MULT));
        printf("%d: I need to 'go'...\n", id);

        // place me in line
        sem_wait(&queue_sem);
        visitor->next = NULL;
        last->next = visitor;
        last = last->next;
        if (first->id == -1) first = first->next;
        sem_post(&queue_sem);

        // Wait for there to be room
        sem_wait(&bathroom);
        while (true) {
            sem_wait(&queue_sem);
            if (first->id != id) {
                // Not my turn, let someone else try
                sem_post(&queue_sem);
                sem_post(&bathroom);
                sem_wait(&bathroom);
            } else {
                // My turn! Let next in line in.
                if (first->next != NULL) first = first->next;
                else {
                    last = &queue;
                    first = &queue;
                    first->next = NULL;
                }
                sem_post(&queue_sem);
                break;
            }
        }

        printf("%d: I'm in!\n", id);
        sleep((unsigned) (rand()%1000 * MULT));

        printf("%d: Back to sl... work(!)\n", id);
        // exit bathroom; semaphore producer
        sem_post(&bathroom);
    }
    pthread_exit(NULL);
}

int main (int argc, char** argv) {
    int goers, i;
    goers = (argc > 1) ? atoi(*(++argv)) : MAXPEOPLE;
    iterations = (argc > 2) ? atoi(*(++argv)) : MAXITERATIONS;
    toilets = (argc > 3) ? atoi(*(++argv)) : MAXTOILETS;

    sem_init(&bathroom, SHARED, (unsigned) toilets);
    sem_init(&queue_sem, SHARED, 1);

    for (i = 0; i < goers; i++)
        pthread_create(&goer_id[i], NULL, goer, (void *) i);

    pthread_exit(NULL);
}
