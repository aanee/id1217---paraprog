import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class Strong_independent_teacher implements Runnable {
    Thread thread;
    private String thread_id;
    private ArrayList<Integer> list_of_ports;
    private Random random;

    Strong_independent_teacher(ArrayList<Integer> port_list) {
        thread_id = "Teacher";
        list_of_ports = port_list;
        random = new Random();
    }

    @Override
    public void run() {
        int i = random.nextInt(list_of_ports.size());
        try {
            Socket sock = new Socket("127.0.0.1", list_of_ports.get(i));
            PrintWriter printWriter = new PrintWriter(sock.getOutputStream());
            list_of_ports.remove(i);
            for (int thing : list_of_ports) printWriter.println(thing);
            printWriter.flush();
            sock.close();
        }
        catch (Exception e) {
            System.out.println("TEACHER IS DEAD");
            return;
        }
        System.out.println(thread_id + ": I'm done here");
    }

    void start() {
        System.out.println(thread_id + " Alive");
        if (thread == null) {
            thread = new Thread(this, thread_id);
            thread.start();
        }
    }
}