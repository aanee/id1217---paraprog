import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class Strong_independent_student implements Runnable {
    Thread thread;
    private String thread_id;
    private int my_port;
    private ArrayList<Integer> list_of_ports;
    private Random random;

    Strong_independent_student(int port) {
        thread_id = Integer.toString(port);
        my_port = port;
        list_of_ports = new ArrayList<>();
        random = new Random();
    }

    @Override
    public void run() {
        String input;
        try {
            ServerSocket serverSocket = new ServerSocket(my_port);
            Socket sock = serverSocket.accept();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            while (true) {
                input = bufferedReader.readLine();
                if (input == null) break;
                if (input.equals("PAL")) {
                    System.out.println(thread_id + " & " + list_of_ports.get(0) + " Sitting in a tree...");
                    sock.close();
                    serverSocket.close();
                    return;
                }
                list_of_ports.add(Integer.parseInt(input));
            }
            sock.close();
            serverSocket.close();

            if (list_of_ports.size() > 0) send_to_friend(false);
            else {
                System.out.println(thread_id + " & " + thread_id + " Sitting in a tree... FOREVER ALONE");
                return;
            }

            if (list_of_ports.size() > 0) send_to_friend(true);
        }
        catch (Exception e) { System.out.println(thread_id + ": DEPRESSION IS KILLING ME"); }
    }

    private void send_to_friend(boolean next) throws Exception {
        int i = random.nextInt(list_of_ports.size());
        int pal = list_of_ports.get(i);
        list_of_ports.remove(i);
        Socket sock = new Socket("127.0.0.1", pal);
        PrintWriter printWriter = new PrintWriter(sock.getOutputStream());
        if (next) for (int thing : list_of_ports) printWriter.println(thing);
        else {
            printWriter.println(thread_id);
            printWriter.println("PAL");
            System.out.println(thread_id + " & " + pal + " Sitting in a tree...");
        }
        printWriter.flush();
        sock.close();
    }

    void start() {
        System.out.println(thread_id + " Alive");
        if (thread == null) {
            thread = new Thread(this, thread_id);
            thread.start();
        }
    }
}
