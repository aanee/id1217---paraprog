/*
 Distributed Pairing 1 (a client-server application) (20 points)

 Assume that a class has n students numbered 1 to n and one teacher. The teacher wants to assign
 the students to groups of two for a project. The teacher does so by having every student submit
 a request for a partner. The teacher takes the first two requests, forms a group from those
 students, and lets each student know his/her partner. The teacher then takes the next two
 requests, forms a second group, and so on. Ifn is odd, the last student "partners" with
 himself/herself.

 Model the students as client processes and the teacher as a server process. Develop and
 implement a distributed client-server application that includes the server (teacher) and clients
 (students). When your application terminates each student process should print his/her own index
 (a number or a name) and the index (a number or a name) of the student's partner. You can
 implement your distributed application in C using the MPI library or in Java using the Java
 socket API or Java RMI. Your application must be distributed. In comments in the program or in a
 README file, shortly explain your solution (algorithm).
 */
/*
 * Students know where teacher is. They all ask teacher for a partner and teacher 
 * take their requests two and two, pairing the students as it goes.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class Teacher {
    private static void doer (String host, int port) throws IOException{
        Socket sock0, sock1;
        PrintWriter wr0, wr1;
        int first_id, second_id;

        ServerSocket serverSocket = new ServerSocket(port);
        serverSocket.setSoTimeout(5000);

        while (true)
            try {
                // First student
                sock0 = serverSocket.accept();
                sock0.setSoTimeout(2000);
                BufferedReader rd0 = new BufferedReader(new InputStreamReader(sock0.getInputStream()));
                first_id = Integer.parseInt(rd0.readLine());
                wr0 = new PrintWriter(sock0.getOutputStream());

                try {
                    // Second student
                    sock1 = serverSocket.accept();
                    sock1.setSoTimeout(2000);
                    BufferedReader rd1 = new BufferedReader(new InputStreamReader(sock1.getInputStream()));
                    second_id = Integer.parseInt(rd1.readLine());
                    wr1 = new PrintWriter(sock1.getOutputStream());
                } catch (Exception s) {
                    wr0.println(first_id);
                    wr0.flush();
                    sock0.close();
                    break;
                }

                // Inform students about their partner
                wr0.println(second_id);
                wr0.flush();
                wr1.println(first_id);
                wr1.flush();

                sock0.close();
                sock1.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                System.out.println("And then the teacher died, THE END.");
                break;
            }
    }

    public static void main (String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("<ip> <port>");
        } else doer(args[0], Integer.parseInt(args[1]));
    }
}
