import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Random;

class Student implements Runnable {
    Thread thread;
    private String thread_id;
    String host;
    int port;

    Student(String id, String _host, int _port) {
        thread_id = id;
        host = _host;
        port = _port;
    }

    @Override
    public void run() {
        try {
            Socket sock = new Socket(host, port);
            sock.setSoTimeout(50000);
            (new PrintStream(sock.getOutputStream())).println(thread_id);
            String partner = (new BufferedReader(new InputStreamReader(sock.getInputStream()))).readLine();
            System.out.println(thread_id + " is in a group with " + partner);
        }catch (Exception e) {}
    }

    void start() {
        System.out.println(thread_id + " Alive");
        if (thread == null) {
            thread = new Thread(this, thread_id);
            thread.start();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        if (args.length < 2) {
            System.out.println("<ip> <port>");
            return;
        }

        String host = args[0];
        int port = Integer.getInteger(args[1]);
        Random rand = new Random();
        Student s = new Student(Integer.toString(rand.nextInt()), host, port);
        s.start();
        s.thread.join();
    }
}