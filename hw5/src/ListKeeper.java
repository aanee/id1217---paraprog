/*
    Determine all common values in three arrays. A.k.a. The Welfare Crook problem (by Prof. D. Gries) (40 points)

    Assume there are three distributed processes: F, G, and H, each having a local array of strings (or integers) :
    f[1:n], g[1:n], and h[1:n], respectively. Assume that the first array f[1:n] is a list that contains names
    (or personal numbers) of people working at IBM Yorktown,  the second g[1:n] is a list of students at Columbia,
    and the third h[1:n] is list of people on welfare in New York City. Assume that there is at least one person whose
    name is on all three lists. [ One can think that something peculiar is going on if the name of the person is on all
    three lists! ].

    Develop and implement a distributed program, in which all three processes interact with each other until each
    process has determined all names, each of which is on all three lists. Each process prints the common names that it
    has determined. Use message passing (or RMI) for interaction between the processes; do not use shared variables.
    Messages may contain only one value fromf, org, orh at a time and (if needed) process ids. Do not send entire arrays
    in messages (or in RMI calls) because the arrays can be huge. You can implement your distributed application in C
    using the MPI library or in Java using the socket API or Java RMI. In comments in the program or in a README file,
    shortly explain your solution (algorithm).
 */

import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class ListKeeper {
    private ArrayList<Integer> pnr;
    private Iterator pnr_iter;
    private ArrayList<Socket> socketList;
    private int num_proceses;
    private boolean hammer_time = true;
    // set up sockets here


    ListKeeper (ArrayList<Integer> data, ArrayList<Socket> listOfSockets) {
        pnr = data;
        pnr_iter = data.iterator();
        socketList = listOfSockets;
        num_proceses = listOfSockets.size();
    }

    void generate_garbage (ArrayList<Integer> list, int size) {
        Random rng = new Random();
        int max = 10000000;
        int min = 1000000;

        list = new ArrayList<Integer>();
        for (int i = 0; i < size; i++) list.add(rng.nextInt((max - min) + 1) + min);
        System.out.println("Garbage data generated");
    }

    void stop() {
        int done = 0;
        int yup = 0;
        int ans = 0;
        int name;

        while (hammer_time) {
            if (ans == 2) name = (int) pnr_iter.next();
        }
    }

    public static void main (String[] args) {
        if (args.length < 3) {
            System.out.println("<Integer(0:2)::id> <String::ip> <Integer::port> <Integer::list size>");
            return;
        }


    }
}
