/*
Teacher tells a student to choose a partner, this student chooses a partner, removes the partner from the list
and sends the list of student to a random next student. This repeets till there are noone left in the list.
and if a student finds itself with an empty list it chooses itself as its partner.
 */

import java.util.ArrayList;
import java.util.Random;

public class Strong_independent_classroom {
    public static void main(String[] args) throws InterruptedException{
        int number_of_students = 11;
        Strong_independent_student[] std_list = new Strong_independent_student[number_of_students];
        ArrayList<Integer> port_list = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < number_of_students; i++) {
            port_list.add(12000+i);
        }

        (new Strong_independent_teacher(port_list)).start();
        for (int i = 0; i < number_of_students; i++) std_list[i] = new Strong_independent_student(port_list.get(i));
        for (int i = 0; i < number_of_students; i++) std_list[i].start();
        for (int i = 0; i < number_of_students; i++) std_list[i].thread.join();
        System.out.println("DONE");
    }
}
