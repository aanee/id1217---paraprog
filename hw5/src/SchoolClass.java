public class SchoolClass {
    public static void main (String[] args) throws InterruptedException {
        if (args.length < 3) {
            System.out.println("<ip> <port> <number of students>");
            return;
        }

        String host = args[0];
        int port = Integer.parseInt(args[1]);
        int i, students = Integer.parseInt(args[2]);
        Student[] teachers_pets = new Student[students];

        for (i = 0; i < students; i++) teachers_pets[i] = new Student(Integer.toString(i), host, port);
        for (i = 0; i < students; i++) teachers_pets[i].start();
        for (i = 0; i < students; i++) teachers_pets[i].thread.join();
    }
}
