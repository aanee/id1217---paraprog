/* matrix summation using OpenMP

   usage with gcc (version 4.2 or higher required):
     gcc -O -fopenmp -o matrixSum-openmp matrixSum-openmp.c
     ./matrixSum-openmp size numWorkers

     Extend the program so that in addition to sum it finds and prints a value
     and a position (indexes) of the maximum element of the matrix, and a
     value and a position of the minimum element of the matrix.  Initialize
     elements of the matrix to random values to check your solution.
     Use OpenMP constructs.
     Change the program developed in (a) so that the master thread prints the
     final results. Do not use explicit barriers and do not use arrays for
     partial results, such as  sums in the pthreads example.

*/

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define MAXSIZE 10000  /* maximum matrix size */
#define MAXWORKERS 8   /* maximum number of workers */

double start_time, end_time;

int numWorkers;
int size;
int matrix[MAXSIZE][MAXSIZE];
int maxs[MAXSIZE][2]; // x, y, sum
int mins[MAXSIZE][2];
int global_max[] = {0,0,0};
int global_min[] = {-1,0,0};

void *Worker(void *);

void check_if_max(int *index) {
  int t = matrix[index[0]][index[1]];
  if (!(t < global_max[0]))
    #pragma omp critical
    if (global_max[0] < t) {
      global_max[0] = t;
      global_max[1] = index[0];
      global_max[2] = index[1];
    }
}

void check_if_min(int *index) {
  int t = matrix[index[0]][index[1]];
  if (!(t > global_min[0]) || global_min[0] == -1) {
    #pragma omp critical
      if (global_min[0] == -1 || global_min[0] > t) {
          global_min[0] = t;
          global_min[1] = index[0];
          global_min[2] = index[1];
      }
  }
}

/* read command line, initialize, and create threads */
int main(int argc, char *argv[]) {
  int i, j, total=0, min, max, mini[2], maxi[2];

  /* read command line args if any */
  size = (argc > 1)? atoi(argv[1]) : MAXSIZE;
  numWorkers = (argc > 2)? atoi(argv[2]) : MAXWORKERS;
  if (size > MAXSIZE) size = MAXSIZE;
  if (numWorkers > MAXWORKERS) numWorkers = MAXWORKERS;

  omp_set_num_threads(numWorkers);

  /* initialize the matrix */
  for (i = 0; i < size; i++) {
    //  printf("[ ");
	  for (j = 0; j < size; j++) {
      matrix[i][j] = rand()%99;
      //	  printf(" %d", matrix[i][j]);
	  }
	  //	  printf(" ]\n");
  }

  start_time = omp_get_wtime();
#pragma omp parallel for reduction (+:total) private(j, mini, maxi)
  for (i = 0; i < size; i++) {
    min = matrix[i][0];
    max = matrix[i][0];
    for (j = 0; j < size; j++) {
      total += matrix[i][j];
      if (max < matrix[i][j]){
        max = matrix[i][j];
        maxi[0] = i;
        maxi[1] = j;
      }
      if (min > matrix[i][j]) {
        min = matrix[i][j];
        mini[0] = i;
        mini[1] = j;
      }
    }
    check_if_max(maxi);
    check_if_min(mini);
  }
// implicit barrier

  end_time = omp_get_wtime();

  printf("the total is %d\n", total);
  printf("it took %g seconds\n", end_time - start_time);
  printf ("min: %d (%d, %d)\nmax: %d (%d, %d)\n", global_min[0], global_min[1], global_min[2],
          global_max[0], global_max[1], global_max[2]);
  //printf("%g\n", end_time - start_time);
}
