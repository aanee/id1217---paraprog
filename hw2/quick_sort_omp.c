#ifndef _REENTRANT
#define _REENTRANT
#endif
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <sys/time.h>
#include <stdbool.h>

char *list;
int size;

double read_timer() {
    static bool initialized = false;
    static struct timeval start;
    struct timeval end;
    if( !initialized )
    {
        gettimeofday( &start, NULL );
        initialized = true;
    }
    gettimeofday( &end, NULL );
    return (end.tv_sec - start.tv_sec) + 1.0e-6 * (end.tv_usec - start.tv_usec);
}

// only uses small letters fo the list
void gen_test_data(int n) {
    list = (char *) malloc(sizeof(char)*n);
    for (int i = 0; i < n; i++)
        *(list + i) = 97 + rand()%26;
}

// check if sorted and number of elements
int check_list() {
    char* tmp_list = list;
    char last = *tmp_list;
    while ((++tmp_list-list) < size)
        if (last > *tmp_list)
            return -1;
    return (int) (tmp_list - list);
}
/*
void swap(int i, int j);
void insert_sort(int left, int right) {
  int index, len=right-left+1;
  char *tmp_list = list;
  for (int i = 1; i < len; i++) {
    index = left + i;
    while (index-- > left && *(tmp_list + index - 1) > *(tmp_list + index))
      swap(index-1, index);
  }
}*/

void swap(int i, int j) {
    char tmp = *(list + i);
    *(list + i) = *(list + j);
    *(list + j) = tmp;
}

int partition(int l, int r, int pivot) {
    int left = l - 1;
    int right = r;
    while (true) {
        while (*(list + ++left) < pivot);
        while (right > 0 && *(list + --right) > pivot);
        if (left >= right) break;
        else swap(left, right);
    }
    swap(left, r);
    return left;
}

void fn (int left, int right) {
    if (right - left <= 0) return;
    else {
        int part_point = partition(left, right, *(list + right));
        #pragma omp task
        {
          fn (left, part_point - 1);
        }
        fn (part_point + 1, right);
    }
}

int main (int argc, char **argv) {
    double start_time, end_time;
    int raw_data = 0, workers = 0;

    if (argc < 2) {
        printf("quick_sort <length of list to sort>\n");
        exit(0);
    } else size = atoi(*(++argv));
    if (argc > 2) raw_data = atoi(*(++argv));
    if (argc > 3) workers = atoi(*(++argv));

    if (workers) omp_set_num_threads(workers);

    gen_test_data(size);

    start_time = omp_get_wtime();
    #pragma omp parallel
    #pragma omp single
    fn(0, size - 1);
    end_time = omp_get_wtime();

    // Raw data switch, to make batchtesting more comfortable
    if (!raw_data) {
      int chk = check_list();
      if (chk == -1) {
          printf("len: %d ERROR\n", size);
          while (*(list++) != NULL) printf("%c", *list);
          printf("\n");
      } else printf("len: %d\n", chk);
      printf("t: %g sec\n\n", end_time-start_time);
    } else printf("%g", end_time-start_time);
}
