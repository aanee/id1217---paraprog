reset
set term png truecolor
set output "quick.png"
set xlabel "List Lenght"
set ylabel "Seconds"
set grid
set auto x
set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
set boxwidth 0.9
plot "quick_data.csv" u 1:2 w boxes lc rgb"red" notitle
