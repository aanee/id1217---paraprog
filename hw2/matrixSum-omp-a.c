/* matrix summation using OpenMP

   usage with gcc (version 4.2 or higher required):
     gcc -O -fopenmp -o matrixSum-openmp matrixSum-openmp.c 
     ./matrixSum-openmp size numWorkers

     Extend the program so that in addition to sum it finds and prints a value
     and a position (indexes) of the maximum element of the matrix, and a
     value and a position of the minimum element of the matrix.  Initialize
     elements of the matrix to random values to check your solution.
     Use OpenMP constructs.

*/

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define MAXSIZE 10000  /* maximum matrix size */
#define MAXWORKERS 8   /* maximum number of workers */

double start_time, end_time;

int numWorkers;
int size; 
int matrix[MAXSIZE][MAXSIZE];
int maxs[MAXSIZE][2]; // x, y, sum
int mins[MAXSIZE][2];

void *Worker(void *);

/* read command line, initialize, and create threads */
int main(int argc, char *argv[]) {
  int i, j, total=0;

  /* read command line args if any */
  size = (argc > 1)? atoi(argv[1]) : MAXSIZE;
  numWorkers = (argc > 2)? atoi(argv[2]) : MAXWORKERS;
  if (size > MAXSIZE) size = MAXSIZE;
  if (numWorkers > MAXWORKERS) numWorkers = MAXWORKERS;

  omp_set_num_threads(numWorkers);

  /* initialize the matrix */
  for (i = 0; i < size; i++) {
    //  printf("[ ");
	  for (j = 0; j < size; j++) {
      matrix[i][j] = rand()%99;
      //	  printf(" %d", matrix[i][j]);
	  }
	  //	  printf(" ]\n");
  }

  /* initialize the min and sum arrays*/
  for (i = 0; i < MAXSIZE; i++) {
    mins[i][0] = 0;
    mins[i][1] = 0;
    mins[i][2] = 0;

    maxs[i][0] = 0;
    maxs[i][1] = 0;
    maxs[i][2] = 0;
  }

  start_time = omp_get_wtime();
#pragma omp parallel for reduction (+:total, maxs, mins) private(j)
  for (i = 0; i < size; i++)
    for (j = 0; j < size; j++){
      total += matrix[i][j];

      if (matrix[i][j] > maxs[i][1]) {
        maxs[i][0] = j;
        maxs[i][1] = matrix[i][j];
      }

      if (matrix[i][j] < mins[i][1]) {
        mins[i][0] = j;
        mins[i][1] = matrix[i][j];
      }
    }
// implicit barrier
  int max_index = 0, min_index = 0;
  for (i = 1; i < size; i++) {
    if (mins[i-1][1] > mins[i][1]) min_index = i;
    if (maxs[i-1][1] < maxs[i][1]) max_index = i;
    //printf("%d %d ", mins[i], maxs[i]);
  }
  end_time = omp_get_wtime();

  //printf("the total is %d\n", total);
  //printf("it took %g seconds\n", end_time - start_time);


  //printf ("min: %d (%d, %d)\nmax: %d (%d, %d)\n", mins[min_index][1], min_index, mins[min_index][0],
  //        maxs[max_index][1], max_index, maxs[max_index][0]);
  printf("%g\n", end_time - start_time);
}

