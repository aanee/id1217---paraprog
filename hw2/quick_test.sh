sh quick_compile.sh
file="quick_data.dat"
echo '' > $file

## 100 1'000 10'000 100'000 1'000'000 10'000'000
for i in 100 1000 10000 100000 1000000 10000000
do
    echo "#$i" >> $file
    p_0=$(./quick $i 1)
    p_1=$(./quick $i 1)
    p_2=$(./quick $i 1)
    p_3=$(./quick $i 1)
    p_4=$(./quick $i 1)
    p_tot=$(python -c "print(($p_0+$p_1+$p_2+$p_3+$p_4)/5)")

    s_0=$(./quick $i 1 1)
    s_1=$(./quick $i 1 1)
    s_2=$(./quick $i 1 1)
    s_3=$(./quick $i 1 1)
    s_4=$(./quick $i 1 1)
    s_tot=$(python -c "print(($s_0+$s_1+$s_2+$s_3+$s_4)/5)")
    
    speed_up=$(python -c "print ($s_tot/$p_tot)")
    echo "$speed_up\n" >> $file
done
