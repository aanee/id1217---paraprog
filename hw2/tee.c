#ifndef _REENTRANT
#define _REENTRANT
#endif
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>

/* tee.c - write text from stdin to stdout and file at the same time */

pthread_t out_std, out_file;
int std_pipe[2];                 // 0 - read, 1 - write
int file_pipe[2];
static volatile int run = 1;
char* name;

void sigint(int sig) {
    run = 0;
}

void* write_std() {
    FILE *pipe = fdopen(std_pipe[0], "r");
    char *c;
    char buf[500];

    while ((c = fgets(buf, 500, pipe)) != NULL)
        fputs(c, stdout);

    fclose(pipe);
}

void* write_file() {
    FILE *fd = fopen(name, "rw");
    FILE *pipe = fdopen(file_pipe[0], "r");
    char *c;
    char buf[500];

    while ((c = fgets(buf, 500, pipe)) != NULL)
        fputs( c, fd);

    fclose(fd);
}

int main(int argc, char *argv[]) {
    signal(SIGINT, sigint);
    // open file, send file descriptor to file thread
    name = argv[1];
    // create 2 pipes
    // create processes that listens to pipe
    // start listen to stdin and send input to pipe

    printf("%d\n", pipe(std_pipe));
    printf("%d\n", pipe(file_pipe));
    FILE *std_p = fdopen(std_pipe[1], "r");
    FILE *file_p = fdopen(file_pipe[1], "r");
    char buf[500];

    pthread_create (&out_std, NULL, (void *) write_std, NULL);
    pthread_create (&out_file, NULL, (void *) write_file, NULL);

    // listening loop here
    while (true) {
        char* c = fgets(buf, 500, stdin);
        fputs(c, std_p);
        fputs(c, file_p);

        // if input is interupt -> exit threads
        if(!run) {
            fputs(NULL, std_p);
            fputs(NULL, file_p);
            pthread_join(out_std, NULL);
            pthread_join(out_file, NULL);
            break;
        }
    }
}
