sh matrix_compile.sh
echo '' > matrix_data.dat
FILE="matrix_data.dat"

# matrix sizes: 100, 1000, 10000
for VAR in matrix-a matrix-b
do
    echo "$VAR" >> $FILE
    for i in 100 1000 10000
    do
        echo "$i" >> $FILE
        s_0=$(./$VAR $i 1)
        s_1=$(./$VAR $i 1)
        s_2=$(./$VAR $i 1)
        s_3=$(./$VAR $i 1)
        s_4=$(./$VAR $i 1)
        s_med=$(python -c "print(($s_0+$s_1+$s_2+$s_3+$s_4)/5)")

        p_0=$(./$VAR $i)
        p_1=$(./$VAR $i)
        p_2=$(./$VAR $i)
        p_3=$(./$VAR $i)
        p_4=$(./$VAR $i)
        p_med=$(python -c "print(($p_0+$p_1+$p_2+$p_3+$p_4)/5)")

        speed_up=$(python -c "print($s_med/$p_med)")
        echo "$speed_up" >> $FILE
    done
    echo "\n" >> $FILE
done
