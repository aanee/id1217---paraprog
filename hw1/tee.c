#ifndef _REENTRANT
#define _REENTRANT
#endif
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <stdbool.h>

/* tee.c - write text from stdin to stdout and file at the same time */

pthread_t out_std, out_file;
pthread_cond_t i0, i1;
static volatile int run = 1;
char* name;
char* input_0[500];
char* input_1[500];

void sigint(int sig) {
    run = 0;
}

void* write_std() {
    char *c;

    while (run) {
      // condition variable which buffer to read from
      // point c at correct buffer
      fputs(c, stdout);
    }

    fclose(pipe);
}

void* write_file() {
    FILE *fd = fopen(name, "rw");
    char *c;

    while (run) {
      // condition variable which buffer to read from
      // point c at correct buffer
      fputs( c, fd);
    }

    fclose(fd);
}

int main(int argc, char *argv[]) {
    signal(SIGINT, sigint);
    name = argv[1];

    pthread_create (&out_std, NULL, (void *) write_std, NULL);
    pthread_create (&out_file, NULL, (void *) write_file, NULL);

    // listening loop here
    while (true) {
        char* c = fgets(buf, 500, stdin);
        fputs(c, std_p);
        fputs(c, file_p);

        // if input is interupt -> exit threads
        if(!run) {
            pthread_join(out_std, NULL);
            pthread_join(out_file, NULL);
            break;
        }
    }
}
