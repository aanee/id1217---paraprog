#ifndef _REENTRANT
#define _REENTRANT
#endif
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <stdbool.h>

#define MAXWORKERS 100

char *list;
int max_threads = MAXWORKERS;
int active_threads = 1;
pthread_mutex_t thread_check = PTHREAD_MUTEX_INITIALIZER;
pthread_t workerid[MAXWORKERS];
long workers = 0;
int size;

double read_timer() {
    static bool initialized = false;
    static struct timeval start;
    struct timeval end;
    if( !initialized )
    {
        gettimeofday( &start, NULL );
        initialized = true;
    }
    gettimeofday( &end, NULL );
    return (end.tv_sec - start.tv_sec) + 1.0e-6 * (end.tv_usec - start.tv_usec);
}

void gen_test_data(int n) {
    list = (char *) malloc(sizeof(char)*n);
    for (int i = 0; i < n; i++)
        *(list + i) = 97 + rand()%26;
}

// check if sorted and number of elements
int check_list() {
    char* tmp_list = list;
    char last = *tmp_list;
    int i = 0;
    while ((++tmp_list-list) < size)
        if (last > *tmp_list)
            return -1;
    return (int) (tmp_list - list);
}

void swap(int i, int j) {
    char tmp = *(list + i);
    *(list + i) = *(list + j);
    *(list + j) = tmp;
}

int partition(int l, int r, int pivot) {
    int left = l - 1;
    int right = r;
    while (true) {
        while (*(list + ++left) < pivot);
        while (right > 0 && *(list + --right) > pivot);
        if (left >= right) break;
        else swap(left, right);
    }
    swap(left, r);
    return left;
}

void* fn (void* arg) {
    int* attris = (int*) arg;
    int left = attris[0];
    int right = attris[1];

    if (right-left <= 0) return;
    else {
        int pivot = *(list + right);
        int part_point = partition(left, right, pivot);

        int arg1[] = {left, part_point-1};
        int arg2[] = {part_point + 1, right};

        // Check if the partitioned lists are long enough to need to be processed
        // Also insertion sort for sub 10 lists.
        long worker_index_1, worker_index_2;
        pthread_mutex_lock(&thread_check);
        if (active_threads < max_threads && workers < MAXWORKERS) {
            active_threads += 1; // this one will be sleeping
            worker_index_1 = workers++;
            worker_index_2 = workers++;
            pthread_mutex_unlock(&thread_check);

            // create only one new thread should be better
            pthread_create(&workerid[worker_index_1], NULL, fn, arg1);
            pthread_create(&workerid[worker_index_2], NULL, fn, arg2);
            pthread_join(workerid[worker_index_1], NULL);
            pthread_join(workerid[worker_index_2], NULL);
        } else {
            pthread_mutex_unlock(&thread_check);
            fn(arg1);
            fn(arg2);
        }
    }
}

int main (int argc, char **argv) {
    double start_time, end_time;
    int test_list_size;

    if (argc < 2) {
        printf("quick_sort <num_of_threads (OPTIOAL DEFAULT: 100)> <length of list to sort>\n");
        exit(0);
    }

    // read in list from stdin
    if (argc > 2) {
        max_threads = atoi(*(++argv));
        test_list_size = atoi(*(++argv));
    } else test_list_size = atoi(*(++argv));
    size = test_list_size;

    gen_test_data(test_list_size);

    int attris[] = {0, test_list_size - 1};
    start_time = read_timer();
    fn(attris);
    end_time = read_timer();

    int chk = check_list();

    if (chk == -1){
      printf("len: %d ERROR\n", test_list_size);
      while (*list != NULL) printf("%c", *(list++));
      printf("\n");
    }
    else printf("len: %d threads: %d\n", chk, max_threads);
    printf("t: %g sec\n\n", end_time-start_time);
}
